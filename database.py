from pymongo import MongoClient

class Database(object):
    """
        An abstraction over the database to allow easy swapping of the
        underlying database. Currently the underlying database is Mongodb, NoSQL
        store.
    """
    def __init__(self, db_name):

        self.db_name = db_name
        self.client = MongoClient()
        self.db = self.client[db_name]

    def clear(self):

        self.client.drop_database(self.db_name)

    def insert(self, d):
        """
            :param d (dict): key-value pairs to be inserted
        """
        if type(d) != dict:
            raise ValueError("Insert only supported on dict")

        self.db.posts.insert_one(d)


    def yield_all(self, query={}):
        """
            Query is a dict containing necessary information to be queried
            from the database.

            Currently, the structure of the query is specific to MongoDB
            Check: http://api.mongodb.com/python/current/tutorial.html#range-queries
            for instructions on specifying advanced queries.
        """
        if type(query) != dict:
            raise ValueError("Query has to be a dict")
        for d in self.db.posts.find(query):
            del d['_id']
            yield d