def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
    
def is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def type_cast(value):

    if is_int(value):
        value = int(value)
    elif is_float(value):
        value = float(value)
    return value