## Dependencies

Please download mongodb latest stable community edition on your system using the link below:
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

The codebase is in python3, everything required is packaged into a virtualenv

Run the following commands to setup the virtualenv:

	virtualenv .env -p python3
	source .env/bin/activate
	pip install -i requirements.txt

## Running the code

The api is written as a flask application and is present in api.py

When running the application for the first time, the data has to be loaded
onto the database. Please run load_data.py to do this.

Once the data is loaded, the api can be accessed by running api.py, the URL
for the api is displayed in the terminal. Either the browser or CURL can be
used to test the api. tests.py also contains code that runs tests on the api.

## API endpoints

Assume the url is given by http://127.0.0.1:5000/, get requests on this url
gives responses for different search queries. The response is returned as a json
string with data as the key, a list of dicts is used to store the data.

1. List all data by sending get request to http://127.0.0.1:5000/
2. Obtain particular compound by requesting http://127.0.0.1:5000/compound/is/Zr1S2
3. Obtain compounds containing a particular element(or compounds) by requesting
	http://127.0.0.1:5000/compound/has/Al or http://127.0.0.1:5000/compound/has/Al2
4. Obtain compounds of a particular color by running http://127.0.0.1:5000/color/is/white
	the name of the color is not case sensitive. Even color supports matching like
	compounds using 'has' where all compounds which are of a particular color
	can be listed regardless of its intensity (eg light gray, dark gray etc)
5. Compounds of a particular bandgap can be queried using
	http://127.0.0.1:5000/bandgap/eq/3.19, instead of eq other operators can be used
	a list of operators are:
		1. eq = equals
		2. lt = less than
		3. lte = less than equal to
		4. gt = greater than
		5. gte = greater than equal to
		6. ne = not equal to

## Thoughts put into the design

It looked like data.csv can have more properties inserted in the future, the code
load_data will work in the presence of any number of new properties as long as the general
structure of having property name followed by value is preserved, even the type of the
new property value is automatically detected during addition. Using a NoSQL database
allows us to have some compounds with extra properties(eg: acidity could be a property of
only compounds which are acidic, I may be wrong with the example).

Having a database abstraction present in database.py allows us to quickly change mongoDB
with some other database, even replace NoSQL with SQL without affecting other parts of the
code. The presence of tests makes this process easier. The database abstraction also has no relation to the properties present in data.csv this further ensures that new properties would not require any change in database.py.

The structure of api.py is such that each property exists as a new function, so new search
operations required for future properties can be easily integrated without requiring a change
of the other code base. If the new property is a string, a function to support regex based
matching is already implemented.

The entire code base is minimally tested. It can certainly be improved in terms of
amount of testing in the future, more thought put into design could also lead to better code.