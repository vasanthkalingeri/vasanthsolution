from utils import type_cast
from database import Database
from pprint import pprint

def add_data(csv_fname, db):
    """
        Load from the csv and store it into the database. The function
        doesn't load the entire csv into memory thereby supporting large csvs
    """
    f = open(csv_fname)
    next(f) # Skip the heading

    for line in f:
        content = line.strip().split(',')
        d = {'Compound': content[0]}

        for i in range(1, len(content), 2): # Based on structure of data
            key = content[i]
            value = content[i + 1]
            d[key] = type_cast(value)
        db.insert(d)
    f.close()

def print_db(db):
    """
        A test function to see the data inserted.
    """
    for d in db.yield_all():
        pprint(d)

if __name__ == '__main__':
    
    DB_NAME = 'citrine'
    CSV_FNAME = 'data.csv'

    db = Database(DB_NAME)
    add_data(CSV_FNAME, db)

    print_db(db)