"""
	Exposes a REST like API to search data on the database. Each property
	has a URL to search with.
"""
from database import Database
from flask import Flask, jsonify

DB_NAME = 'citrine'
JSON_DATA_KEY = 'data'

app = Flask(__name__)
app.config['DB_NAME'] = DB_NAME

def create_string_query(data_type, operator, value):
	"""
		Properties which are strings can be searched as exact matches or
		regex matches. This function creates the appropriate structure
		accepted by Database to enable such search.
	"""
	if operator == 'is':
		value = value
	elif operator == 'has':
		value = {'$regex': value}
		
	return {data_type: value}

@app.route('/', methods=['GET'])
def get_data():
	"""
		Lists all the data in the database
	"""
	db = Database(app.config['DB_NAME'])
	data = list(db.yield_all())
	return jsonify({JSON_DATA_KEY: data})

@app.route('/compound/<operator>/<compound>', methods=['GET'])
def query_by_compound(operator, compound):
	"""
		operator:
			has: query as a regex
			is: obtain exact matches

		compound:
			name of the compound to query by
	"""
	db = Database(app.config['DB_NAME'])
	query = create_string_query('Compound', operator, compound)
	data = list(db.yield_all(query))
	return jsonify({JSON_DATA_KEY: data})

@app.route('/bandgap/<operator>/<float:bandgap>', methods=['GET'])
def query_by_bandgap(operator, bandgap):
	"""
		operator:
			eq: obtain data with matching bandgap
			lt: obtain data with bandgap less than a particular value
			lte: obtain data with bandgap less than equal to
			gt: greater than
			gte: greater than equal to
			ne: not equal to
	"""
	db = Database(app.config['DB_NAME'])
	query = {'$' + operator: bandgap}
	data = list(db.yield_all({'Band gap': query}))
	return jsonify({JSON_DATA_KEY: data})

@app.route('/color/<operator>/<color>', methods=['GET'])
def query_by_color(operator, color):
	"""
		Similar to query by compound
	"""
	db = Database(app.config['DB_NAME'])
	color = color.title() # Since data stored in this format
	query = create_string_query('Color', operator, color)
	data = list(db.yield_all(query))
	return jsonify({JSON_DATA_KEY: data})

if __name__ == '__main__':
	app.run()