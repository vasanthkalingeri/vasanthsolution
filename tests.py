import json
import unittest

from load_data import add_data
from utils import type_cast
from database import Database
from api import app

CSV_FNAME = 'test_data.csv'
DB_NAME = 'test_citrine'

def load_test_data(csv_fname):
    """
        Returns a list of dicts of the original data
    """
    f = open(csv_fname)
    next(f)
    data = []
    for line in f:
        content = line.strip().split(',')
        d = {'Compound': content[0]}

        for i in range(1, len(content), 2): # Based on structure of data
            key = content[i]
            value = content[i + 1]
            d[key] = type_cast(value)
        data.append(d)
    f.close()
    return data

class TestData(unittest.TestCase):
    """
        To keep tests simple, taking for granted that clear and init are
        correctly implemented and work fine
    """
    def setUp(self):
        self.db = Database(DB_NAME)
        self.db.clear()
        # Testing by inserting some data
        self.test_data = load_test_data(CSV_FNAME)

    def test_clear(self):

        for i in range(5):
            self.db.insert(self.test_data[i])
        self.db.clear()
        data = list(self.db.yield_all())
        self.assertTrue(len(data) == 0)

    def test_add_data_normal_operation(self):

        add_data(CSV_FNAME, self.db)
        data = list(self.db.yield_all())
        self.assertListEqual(data, self.test_data)

class TestApi(unittest.TestCase):
    """
        Tests if the search interface works fine
    """
    def setUp(self):

        app.testing = True
        self.app = app.test_client()
        app.config['DB_NAME'] = DB_NAME
        self.db = Database(DB_NAME)
        self.db.clear()
        self.test_data = load_test_data(CSV_FNAME)

    def _parse_response(self, response):
        return json.loads(response.get_data(as_text=True))['data']

    def test_all_data(self):

        add_data(CSV_FNAME, self.db)
        response = self.app.get('/')
        data = self._parse_response(response)
        self.assertListEqual(data, self.test_data)

    def test_get_exact_compound_matches(self):

        add_data(CSV_FNAME, self.db)
        response = self.app.get('/compound/is/Zr1S2')
        data = self._parse_response(response)
        self.assertListEqual(data, [self.test_data[1]])

    def test_get_regex_compound_matches(self):

        add_data(CSV_FNAME, self.db)
        response = self.app.get('/compound/has/Al')
        data = self._parse_response(response)
        al_test_data = [t for t in self.test_data if t['Compound'][:2] == 'Al']
        self.assertListEqual(data, al_test_data)

    def test_get_exact_color_matches(self):
        
        add_data(CSV_FNAME, self.db)
        trials = ['white', 'WHITE', 'WHite', 'White']
        white_data = [t for t in self.test_data if t['Color'] == 'White']

        for trial in trials:
            response = self.app.get('/color/is/' + trial)
            data = self._parse_response(response)    
            self.assertListEqual(data, white_data)

    def test_get_regex_color_matches(self):
        
        add_data(CSV_FNAME, self.db)
        trials = ['white', 'WHITE', 'WHite', 'White']
        white_data = [t for t in self.test_data if t['Color'].find('White') >= 0]
        for trial in trials:
            response = self.app.get('/color/has/' + trial)
            data = self._parse_response(response)
            self.assertListEqual(data, white_data)

    def test_bandgap_equal_to(self):
        
        add_data(CSV_FNAME, self.db)
        response = self.app.get('/bandgap/eq/3.19')
        data = self._parse_response(response)
        test_data = [t for t in self.test_data if t['Band gap'] == 3.19]
        self.assertListEqual(data, test_data)

    def test_bandgap_less_than(self):
        
        add_data(CSV_FNAME, self.db)
        response = self.app.get('/bandgap/lt/3.1')
        data = self._parse_response(response)
        test_data = [t for t in self.test_data if t['Band gap'] < 3.1]
        self.assertListEqual(data, test_data)

    def test_bandgap_less_than_equal_to(self):
        
        add_data(CSV_FNAME, self.db)
        response = self.app.get('/bandgap/lte/3.19')
        data = self._parse_response(response)
        test_data = [t for t in self.test_data if t['Band gap'] <= 3.19]
        self.assertListEqual(data, test_data)

    def test_bandgap_greater_than_equal_to(self):
        
        add_data(CSV_FNAME, self.db)
        response = self.app.get('/bandgap/gte/1.6')
        data = self._parse_response(response)
        test_data = [t for t in self.test_data if t['Band gap'] >= 1.6]
        self.assertListEqual(data, test_data)

    def test_bandgap_greater_than(self):
        
        add_data(CSV_FNAME, self.db)
        response = self.app.get('/bandgap/gt/1.6')
        data = self._parse_response(response)
        test_data = [t for t in self.test_data if t['Band gap'] > 1.6]
        self.assertListEqual(data, test_data)

    def test_bandgap_not_equal_to(self):
        
        add_data(CSV_FNAME, self.db)
        response = self.app.get('/bandgap/ne/3.19')
        data = self._parse_response(response)
        test_data = [t for t in self.test_data if t['Band gap'] != 3.19]
        self.assertListEqual(data, test_data)

if __name__ == '__main__':
    unittest.main()